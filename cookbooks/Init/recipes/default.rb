
# Cookbook:: Init
# Recipe:: default

# Copyright:: 2019, The Authors, All Rights Reserved.

apt_update 'Update apt cache' do
  ignore_failure true
  action :update
end

apt_package 'Install prerequisite tools' do
  package_name ['apt-transport-https', 'curl', 'ca-certificates', 'gnupg2', 'software-properties-common']
  action :install
end

apt_repository 'Add K8s apt source list' do
  repo_name 'kubernetes'
  uri 'https://apt.kubernetes.io/'
  distribution 'kubernetes-xenial'
  components ['main']
  key 'https://packages.cloud.google.com/apt/doc/apt-key.gpg'
  action :add
end

apt_package 'Install kubelet kubeadm kubectl' do
  package_name ['kubelet', 'kubeadm', 'kubectl']
end

apt_repository 'Add Docker apt source list' do
  repo_name 'docker'
  uri 'https://download.docker.com/linux/ubuntu'
  arch 'amd64'
  components ['stable']
  key 'https://download.docker.com/linux/ubuntu/gpg'
  action :add
end

apt_package 'Install docker' do
  version ['18.06.1~ce~3-0~ubuntu']
  package_name ['docker-ce']
end

systemd_unit 'start and enable Docker daemon' do
  unit_name 'docker.service'
  action [:start, :enable]
end