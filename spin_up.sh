#!/bin/bash

#Description: spins up the k8s cluster with IP addresses from file `cluster_ip`

#Read IP addreses from file ip_cluster
TEMP_COUNT=0
while read -r LINE; do 
    A[$TEMP_COUNT]=$(echo $LINE | awk '{ print $2}'); 
    TEMP_COUNT=$((TEMP_COUNT+1));
done <<< $(cat cluster_ip)

K8S_MASTER_IP="${A[0]}"
K8S_NODE1_IP="${A[1]}"
K8S_NODE2_IP="${A[2]}"

echo "K8s Master is $K8S_MASTER_IP"
echo "K8s Node1 is $K8S_NODE1_IP"
echo "K8s Node2 is $K8S_NODE2_IP"

#Install policy
chef install  policyfile/K8sMasterPolicyfile.rb
chef install  policyfile/K8sNodePolicyfile.rb

#Push policy onto server
chef push K8sMasterGroup policyfile/K8sMasterPolicyfile.rb
chef push K8sNodeGroup policyfile/K8sNodePolicyfile.rb

#Bootstrap nodes (setting up nodes so they can be managed with Chef)
knife bootstrap "$K8S_MASTER_IP"  -x ubuntu -i private-key.pem --sudo -N master --yes
knife bootstrap "$K8S_NODE1_IP"   -x ubuntu -i private-key.pem --sudo -N node1 --yes
knife bootstrap "$K8S_NODE2_IP"   -x ubuntu -i private-key.pem --sudo -N node2 --yes

#Set policy for agents
knife node policy set master K8sMasterGroup K8sMasterPolicy
knife node policy set node1 K8sNodeGroup K8sNodePolicy
knife node policy set node2 K8sNodeGroup K8sNodePolicy

#Tell nodes to pull down and apply configs from server
knife ssh "policy_group:K8sMasterGroup" -i private-key.pem "sudo chef-client"
knife ssh "policy_group:K8sNodeGroup" -i private-key.pem "sudo chef-client"
