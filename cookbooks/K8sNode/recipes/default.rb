#
# Cookbook:: K8sNode
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

kubeadm_join_command = search(:node,'policy_group:K8sMasterGroup').first['k8s_join_command']

execute 'Run kubeadm join command' do
    command "#{kubeadm_join_command}"
    action :run
end
