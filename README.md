# chef-start-k8s-cluster

## Discription:
Use Chef to bootstrap a Kubernetes cluster on **existing** infrastructure. This project assumes that the following infrastructure is available to set up the Kubernetes cluster:
* One Ubuntu 16.04 64bit - 1 CPU 2GB RAM (or higher) machine for K8s master
* Two Ubuntu 16.04 64bit - 1 CPU 2GB RAM (or higher) machines for K8s nodes

**NOTE** A Unix user `ubuntu` that can invoke `sudo` command without password is needed. This user must have the same name accross the 3 machines.

These machines are separated from the Chef Serer and Chef Workstation.

## Project Structure

```
.
├── README.md
├── cluster_ip # file to put in your cluster's  IP addresses 
├── cookbooks
│   ├── Init
│   ├── K8sMaster
│   ├── K8sNode
│   └── chefignore
├── private-key.pem # ssh private key to use with the infrastructure
├── policyfile
│   ├── K8sMasterPolicyfile.rb
│   └── K8sNodePolicyfile.rb
└── spin_up.sh # start script
```

## Usage

### 1. Set up a working Chef Server and Workstation

Please refer to the official documenet on how to set up the Chef Server and Workstation. The following steps assume that a working Chef Server and Workstation have been set up.

### 2. Modify IP addresses of machines in the cluster

Edit the file `cluster_ip` to reflect your infrastructure.

### 3. Modify IP addresses of machines in the cluster

Edit the content of the file `private-key.pem` to the private key for use with the machines in the cluster.

### 4. Run the script
   1. Change permission of the file `spin_up.sh` to 700
   2. Change permission of the `private-key.pem` to 400
   3. Run the script `spin_up.sh`

   
