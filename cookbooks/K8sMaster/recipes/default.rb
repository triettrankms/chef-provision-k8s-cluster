#
# Cookbook:: K8sMaster
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.


execute 'Initialize kubeadm' do
    user 'ubuntu'
    command "sudo kubeadm init #{node['kubeadm_init_flags']}"
    action :run
end

ruby_block "Get kubeadm join command" do
    block do
      Chef::Resource::RubyBlock.send(:include, Chef::Mixin::ShellOut)      
      command = 'kubeadm token create --print-join-command'
      command_out = shell_out(command)
      node.normal['k8s_join_command'] = command_out.stdout
    end
    action :run
end  

script 'Config for non-root user' do
    user 'ubuntu'
    interpreter 'bash'
    code <<-EOH
    mkdir -p /home/ubuntu/.kube
    sudo cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
    sudo chown -R ubuntu:ubuntu /home/ubuntu/
    EOH
end

script 'Install pod network add on' do
    user 'ubuntu'
    interpreter 'bash'
    code <<-EOH
    kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
    kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml
    EOH
end